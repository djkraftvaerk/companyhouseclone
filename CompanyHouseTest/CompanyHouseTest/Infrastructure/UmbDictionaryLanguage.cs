﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace CompanyHouseTest.Infrastructure
{
    public class UmbDictionaryLanguage
    {
        public static string GetDanishTranslation(string dictionaryKeyValue)
        {
            ILocalizationService service = ApplicationContext.Current.Services.LocalizationService;
            IDictionaryItem dictionaryItem = service.GetDictionaryItemByKey(dictionaryKeyValue);
            var translations = dictionaryItem.Translations;

            string danishTranslation = "";
            foreach (var translation in translations)
            {
                if (translation.Language.CultureName == "Danish (Denmark)")
                {
                    danishTranslation = translation.Value;
                }
            }

            return danishTranslation;
        }
    }
}