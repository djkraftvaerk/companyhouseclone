﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CompanyHouseTest.Models;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace CompanyHouseTest.Extensions
{
    public static class PublishedContentExtensions
    {
        public static TaskViewModel AsTaskViewModel(this IPublishedContent publishedContent)
        {
            return new TaskViewModel
            {
                ContentId = publishedContent.Id,
                FullName = publishedContent.GetPropertyValue<String>("fullName"),
                PhoneNumber = publishedContent.GetPropertyValue<String>("phoneNumber"),
                TaskName = publishedContent.GetPropertyValue<String>("taskName"),
                TaskDescription = publishedContent.GetPropertyValue<String>("taskDescription"),
                LocationInBuilding = publishedContent.GetPropertyValue<String>("locationInBuilding"),
                TaskStatus = publishedContent.GetPropertyValue<string>("taskStatus"),
                TaskPriority = publishedContent.GetPropertyValue<string>("taskPriority"),
                TaskDate = publishedContent.GetPropertyValue<DateTime>("taskDate")
            };
        }
    }
}