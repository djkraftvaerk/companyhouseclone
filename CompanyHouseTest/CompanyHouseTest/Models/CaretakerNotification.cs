﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyHouseTest.Models
{
    public class CaretakerNotification
    {
        public string CaretakerName { get; set; }
        public string CaretakerEmail { get; set; }
        public string CaretakerMessage { get; set; }
    }
}