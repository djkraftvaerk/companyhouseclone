﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using PagedList;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace CompanyHouseTest.Models
{
    public class TaskRenderModel : RenderModel
    {
        public TaskRenderModel() : this(new UmbracoHelper(UmbracoContext.Current).TypedContent(UmbracoContext.Current.PageId)) { }

        public TaskRenderModel(IPublishedContent content) : base(content)
        {
        }

        public TaskViewModel TaskViewModel { get; set; }

        public IPagedList<TaskViewModel> TasksPaged { get; set; }

        public CaretakerNotification CaretakerNotification { get; set; }

    }
}