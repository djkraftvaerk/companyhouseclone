﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;

namespace CompanyHouseTest.Models
{
    public class TaskViewModel 
    {
        public Int32 ContentId { get; set; }

        [DisplayName("Name")]
        [Required(ErrorMessage = "Full Name is needed.")]
        public string FullName { get; set; }

        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone Number is needed.")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((\(?\+45\)?)?)(\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$", ErrorMessage = "Invalid Phone number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Task Name")]
        [Required(ErrorMessage = "Task Name needed.")]
        public string TaskName { get; set; }

        [DisplayName("Task Description")]
        [Required(ErrorMessage = "Task Description needed.")]
        public string TaskDescription { get; set; }

        [DisplayName("Location In Building")]
        [Required(ErrorMessage = "Location in building needed.")]
        public string LocationInBuilding { get; set; }

        [DisplayName("Task Status")]
        public string TaskStatus { get; set; }

        [DisplayName("Task Priority")]
        public string TaskPriority { get; set; }

        [DisplayName("Task Status")]
        public List<SelectListItem> Statuses { get; set; }

        [DisplayName("Task Priority")]
        public List<SelectListItem> Priorities { get; set; }

        [DisplayName("Task Date")]
        [DataType(DataType.Date)]
        public DateTime TaskDate { get; set; }

        public IEnumerable<IPublishedContent> Tasks { get; set; }
    }
}