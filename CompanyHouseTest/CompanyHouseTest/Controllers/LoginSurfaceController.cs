﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CompanyHouseTest.Controllers
{
    public class LoginSurfaceController : SurfaceController
    {
        // GET: Login
        public ActionResult Index()
        {
            return PartialView("LoginPartial", new LoginModel());
        }

        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            if (Members.Login(model.Username, model.Password))
                return RedirectToUmbracoPage(1114);

            ModelState.AddModelError("", "Invalid Login");

            return CurrentUmbracoPage();
        }

        public ActionResult Logout()
        {
            Members.Logout();
            return RedirectToUmbracoPage(1114);
        }
    }
}