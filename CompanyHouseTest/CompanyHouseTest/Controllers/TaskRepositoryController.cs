﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using CompanyHouseTest.Extensions;
using CompanyHouseTest.Models;
using PagedList;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CompanyHouseTest.Controllers
{
    public class TaskRepositoryController : RenderMvcController
    {
        // GET: TaskRepository
        public ActionResult TaskRepository(RenderModel model, string priority, string status, string date, int? page)
        {

            var taskRepository = CurrentPage;

            ViewBag.TaskRepository = taskRepository;

            IEnumerable<TaskViewModel> tasks = taskRepository.Children("task").Select(t => t.AsTaskViewModel()).OrderByDescending(t => t.TaskDate);
            ViewBag.CurrentDate = "Desc";

            switch (priority)
            {
                case "Low":
                    tasks = tasks.Where(t => t.TaskPriority == "Low");
                    ViewBag.CurrentPriority = "Low";
                    break;
                case "Normal":
                    tasks = tasks.Where(t => t.TaskPriority == "Normal");
                    ViewBag.CurrentPriority = "Normal";
                    break;
                case "High":
                    tasks = tasks.Where(t => t.TaskPriority == "High");
                    ViewBag.CurrentPriority = "High";
                    break;
            }

            switch (status)
            {
                case "On It":
                    tasks = tasks.Where(t => t.TaskStatus == "On It");
                    ViewBag.CurrentStatus = "On It";
                    break;
                case "Waiting":
                    tasks = tasks.Where(t => t.TaskStatus == "Waiting");
                    ViewBag.CurrentStatus = "Waiting";
                    break;
                case "Finished":
                    tasks = tasks.Where(t => t.TaskStatus == "Finished");
                    ViewBag.CurrentStatus = "Finished";
                    break;
            }

            switch (date)
            {
                case "Asc":
                    tasks = tasks.OrderBy(t => t.TaskDate);
                    ViewBag.CurrentDate = "Asc";
                    break;
                case "Desc":
                    tasks = tasks.OrderByDescending(t => t.TaskDate);
                    ViewBag.CurrentDate = "Desc";
                    break;
            }

            int pageSize = 6;
            int pageNumber = (page ?? 1);

            var taskRenderModel = new TaskRenderModel();

            taskRenderModel.TasksPaged = tasks.ToPagedList(pageNumber, pageSize);

     
            return CurrentTemplate(taskRenderModel);
        }
    }
}