﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyHouseTest.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CompanyHouseTest.Controllers
{
    public class TaskUpdateController : RenderMvcController
    {
        public ActionResult TaskUpdate(RenderModel model, int? id)
        {
            if (!id.HasValue)
                return Content("No task found");

            var taskRenderModel = new TaskRenderModel();

            IContent taskById = Services.ContentService.GetById(id ?? 0);

            if (taskById == null)
            {
                return HttpNotFound();
            }

            var taskViewModel = new TaskViewModel();
            taskViewModel.ContentId = taskById.Id;
            taskViewModel.FullName = taskById.GetValue<String>("fullName");
            taskViewModel.PhoneNumber = taskById.GetValue<String>("phoneNumber");
            taskViewModel.TaskName = taskById.GetValue<String>("taskName");
            taskViewModel.TaskDescription = taskById.GetValue<String>("taskDescription");
            taskViewModel.LocationInBuilding = taskById.GetValue<String>("locationInBuilding");
            taskViewModel.TaskStatus = taskById.GetValue<string>("taskStatus");
            taskViewModel.TaskPriority = taskById.GetValue<string>("taskPriority");
            taskViewModel.TaskDate = taskById.GetValue<DateTime>("taskDate"); //{2/28/2019 12:00:00 AM}

            taskRenderModel.TaskViewModel = taskViewModel;

            IDataTypeService dataTypeService = Services.DataTypeService;

            // Task Status DropDown
            IDataTypeDefinition dataTypeStatus = dataTypeService.GetDataTypeDefinitionByName("TaskStatusDropdown");
            var preValuesStatus = dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeStatus.Id);

            var selectListStatus = new List<SelectListItem>();

            foreach (var item in preValuesStatus.PreValuesAsDictionary.Where(x => x.Value.Value != "0"))
            {
                selectListStatus.Add(new SelectListItem()
                    {
                        Disabled = false,
                        Selected = false,
                        Text = item.Value.Value,
                        Value = item.Value.Id.ToInvariantString()
                    }
                );
            }

            // Task Priority DropDown
            IDataTypeDefinition dataTypePriority = dataTypeService.GetDataTypeDefinitionByName("TaskPriorityDropdown");
            var preValuesPriority = dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypePriority.Id);

            var selectListPriority = new List<SelectListItem>();

            foreach (var item in preValuesPriority.PreValuesAsDictionary.Where(x => x.Value.Value != "0"))
            {
                selectListPriority.Add(new SelectListItem()
                    {
                        Disabled = false,
                        Selected = false,
                        Text = item.Value.Value,
                        Value = item.Value.Id.ToInvariantString()
                    }
                );
            }

            taskRenderModel.TaskViewModel.Statuses = selectListStatus;
            taskRenderModel.TaskViewModel.Priorities = selectListPriority;

            var updateTaskPage = CurrentPage;
            ViewBag.UpdateTaskPage = updateTaskPage;

            return CurrentTemplate(taskRenderModel);
        }
    }
}