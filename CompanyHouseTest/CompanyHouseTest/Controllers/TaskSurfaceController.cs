﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CompanyHouseTest.Models;
using Microsoft.Owin.Security.Provider;
using Sms.ApiClient.V2;
using Sms.ApiClient.V2.SendMessages;
using umbraco.NodeFactory;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CompanyHouseTest.Controllers
{
    public class TaskSurfaceController : SurfaceController
    {
        public ActionResult UpdateTaskRedirect(int id)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("id", id.ToString());
            return RedirectToUmbracoPage(1117, nameValueCollection);
        }

        public ActionResult SortByPriority(string priority)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("priority", priority);
            return RedirectToUmbracoPage(1114, nameValueCollection);
        }

        public ActionResult SortByStatus(string status)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("status", status);
            return RedirectToUmbracoPage(1114, nameValueCollection);
        }

        public ActionResult OrderByDate(string date)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("date", date);
            return RedirectToUmbracoPage(1114, nameValueCollection);
        }

        public ActionResult LoadMoreTasks(string loadMore)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("loadMore", loadMore);
            return RedirectToUmbracoPage(1114, nameValueCollection);
        }

        public ActionResult Page(int page, string priority, string status, string date)
        {
            var nameValueCollection = new NameValueCollection();
            nameValueCollection.Add("page", page.ToString());
            nameValueCollection.Add("status", status);
            nameValueCollection.Add("date", date);
            nameValueCollection.Add("priority", priority);

            return RedirectToUmbracoPage(1114, nameValueCollection);
        }


        /*public ActionResult GetTasks(int id, string fetch)
        {
            IPublishedContent taskRepository = Umbraco.TypedContent(1114);
            List<IPublishedContent> tasks = taskRepository.Children.ToList();


        }*/

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateTask(TaskRenderModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            IContent task = Services.ContentService.CreateContent("Task", 1114, "task");
            task.SetValue("fullName", model.TaskViewModel.FullName);
            task.SetValue("phoneNumber", model.TaskViewModel.PhoneNumber);
            task.SetValue("taskName", model.TaskViewModel.TaskName);
            task.SetValue("taskDescription", model.TaskViewModel.TaskDescription);
            task.SetValue("locationInBuilding", model.TaskViewModel.LocationInBuilding);
            task.SetValue("taskStatus", model.TaskViewModel.TaskStatus); 
            task.SetValue("taskPriority", model.TaskViewModel.TaskPriority); 
            task.SetValue("taskDate", model.TaskViewModel.TaskDate);
            Services.ContentService.SaveAndPublishWithStatus(task);

            var caretaker = Services.MemberService.GetById(1168);
            var caretakerNumberString = caretaker.GetValue<string>("phoneNumber");

            var caretakerEmail = caretaker.GetValue<string>("email");

            var caretakerFirstName = caretaker.GetValue<string>("firstName");
            var caretakerLastName = caretaker.GetValue<string>("lastName");
            var caretakerFullName = caretakerFirstName + " " + caretakerLastName;

            var homepage = Services.ContentService.GetById(1069);
            var address = homepage.GetValue("address");

            // Instantiate the client to use
            // NOTE: The api key can be found on top of the documentation page
            var smsClient = new FacadeSmsClient(
                hostRootUrl: "https://mm.inmobile.dk",
                apiKey: "410464b4-17f8-449b-a0bc-665ec285f35f");

            // Create a list of messages to be sent
            var messagesToSend = new List<ISmsMessage>();
            var message = new SmsMessage(
                msisdn: caretakerNumberString, // The mobile number including country code
                text: String.Format("{0}, Task: {1}, created {2}", model.TaskViewModel.FullName, model.TaskViewModel.TaskName, model.TaskViewModel.TaskDate.ToShortDateString()),
                senderName: (string) address, // i.e. 1245 or FancyShop
                encoding: SmsEncoding.Gsm7);
            messagesToSend.Add(message);

            // Send the messages and evaluate the response
            try
            {
                var response = smsClient.SendMessages(
                    messages: messagesToSend,
                    messageStatusCallbackUrl: "http://mywebsite.com/example/messagestatus");
            }
            catch (SendMessageException smex)
            {
                // Catch exception to see error
                Console.WriteLine(smex.Message);
            }

            var caretakerNotification = new CaretakerNotification();
            caretakerNotification.CaretakerEmail = caretakerEmail;
            caretakerNotification.CaretakerName = caretakerFullName;
            caretakerNotification.CaretakerMessage = String.Format("{0}, Task: {1}, created {2}",
                model.TaskViewModel.FullName, model.TaskViewModel.TaskName,
                model.TaskViewModel.TaskDate.ToShortDateString());
            
            // Caretaker Email Notification
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var mailMessage = new MailMessage();
            mailMessage.To.Add(new MailAddress(caretakerNotification.CaretakerEmail));  // replace with valid value 
            mailMessage.From = new MailAddress("sender@outlook.com");  // replace with valid value
            mailMessage.Subject = model.TaskViewModel.TaskName;
            mailMessage.Body = string.Format(body, model.TaskViewModel.FullName, "sender@outlook.com", caretakerNotification.CaretakerMessage);
            mailMessage.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                smtp.Credentials = new System.Net.NetworkCredential("dgrjacobsen@gmail.com", "dglavind12");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(mailMessage);
            }

            return RedirectToUmbracoPage(1114);
        }

        [HttpPost]
        public ActionResult UpdateTask(TaskRenderModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            IContent task = Services.ContentService.GetById(model.TaskViewModel.ContentId);
            if (task != null) { 
                task.SetValue("fullName", model.TaskViewModel.FullName);
                task.SetValue("phoneNumber", model.TaskViewModel.PhoneNumber);
                task.SetValue("taskName", model.TaskViewModel.TaskName);
                task.SetValue("taskDescription", model.TaskViewModel.TaskDescription);
                task.SetValue("locationInBuilding", model.TaskViewModel.LocationInBuilding);
                task.SetValue("taskStatus", model.TaskViewModel.TaskStatus); 
                task.SetValue("taskPriority", model.TaskViewModel.TaskPriority); 
                task.SetValue("taskDate", model.TaskViewModel.TaskDate);
                Services.ContentService.SaveAndPublishWithStatus(task);
            }
            return RedirectToUmbracoPage(1114);
        }

        public ActionResult DeleteTask(int id, int pageId)
        {
            var publishedContent = Umbraco.TypedContent(id);
            var isTask = publishedContent.DocumentTypeAlias.Equals("task");
            if (!isTask) return Content("Only tasks can be deleted!");

            IContent cont = Services.ContentService.GetById(id);
            if (cont != null)
            {
                Services.ContentService.Delete(cont);
            }


            return RedirectToUmbracoPage(pageId);
        }
    }
}