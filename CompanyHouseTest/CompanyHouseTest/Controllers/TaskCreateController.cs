﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyHouseTest.Models;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace CompanyHouseTest.Controllers
{
    public class TaskCreateController : RenderMvcController
    {
        public ActionResult TaskCreate(RenderModel model)
        {
            var taskRenderModel = new TaskRenderModel {TaskViewModel = new TaskViewModel()};

            IDataTypeService dataTypeService = Services.DataTypeService;

            // Task Status DropDown
            IDataTypeDefinition dataTypeStatus = dataTypeService.GetDataTypeDefinitionByName("TaskStatusDropdown");
            var preValuesStatus = dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypeStatus.Id);

            var selectListStatus = new List<SelectListItem>();

            foreach (var item in preValuesStatus.PreValuesAsDictionary.Where(x => x.Value.Value != "0"))
            {
                selectListStatus.Add(new SelectListItem()
                    {
                        Disabled = false,
                        Selected = false,
                        Text = item.Value.Value, //"On It"
                        Value = item.Value.Id.ToInvariantString() //"33"
                        //Value = item.Value.Value
                    }
                );
            }

            // Task Priority DropDown
            IDataTypeDefinition dataTypePriority = dataTypeService.GetDataTypeDefinitionByName("TaskPriorityDropdown");
            var preValuesPriority = dataTypeService.GetPreValuesCollectionByDataTypeId(dataTypePriority.Id);

            var selectListPriority = new List<SelectListItem>();

            foreach (var item in preValuesPriority.PreValuesAsDictionary.Where(x => x.Value.Value != "0"))
            {
                selectListPriority.Add(new SelectListItem()
                    {
                        Disabled = false,
                        Selected = false,
                        Text = item.Value.Value,
                        Value = item.Value.Id.ToInvariantString()
                        //Value = item.Value.Value
                    }
                );
            }

            taskRenderModel.TaskViewModel.Statuses = selectListStatus;
            taskRenderModel.TaskViewModel.TaskStatus = "34";
            taskRenderModel.TaskViewModel.Priorities = selectListPriority;
            taskRenderModel.TaskViewModel.TaskDate = DateTime.Now;

            var createTaskPage = CurrentPage;
            ViewBag.CreateTaskPage = createTaskPage;

            return CurrentTemplate(taskRenderModel);
        }
    }
}